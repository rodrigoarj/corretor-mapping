package cafeit.corretor.ui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cafeit.corretor.dao.FieldDao;
import cafeit.corretor.domain.data.InsuranceCompany;
import cafeit.corretor.domain.field.Field;
import cafeit.corretor.domain.metamodel.FindElementBy;
import cafeit.corretor.domain.metamodel.SourceField;
import cafeit.corretor.domain.metamodel.SourceMandatoryCondition;
import cafeit.corretor.domain.metamodel.SourceUIType;
import cafeit.corretor.domain.metamodel.SourceUITypeSelect;
import cafeit.corretor.domain.metamodel.SourceUITypeSelectItem;
import cafeit.corretor.util.JPAUtil;



public class InspectFormUI extends JFrame {

	private static final String SELECT_TYPE = "Select";

	/**
	 * 
	 */
	private static final long serialVersionUID = 8070362120384393256L;

	private JComboBox<ComboHolder<InsuranceCompany>> ciaCombo = new JComboBox<>();
	private JComboBox<ComboHolder<FindElementBy>> locateCombo = new JComboBox<>();
	private JComboBox<ComboHolder<Field>> comboLabelSeguroLink = new JComboBox<>();
	private JTextField ufIdTF = new JTextField(30);
	private JComboBox<ComboHolder<SourceUIType>> ufTypeCompo = new JComboBox<ComboHolder<SourceUIType>>();
	private JTextField ufTypeTF = new JTextField(20);
	private JTextField ufOrderTF = new JTextField(3);
	private JCheckBox ufCheck = new JCheckBox("Mandatory");
	private JComboBox<ComboHolder<SourceField>> mcCombo = new JComboBox<ComboHolder<SourceField>>();
	private JTextField mcTF = new JTextField(15);

	private EntityManager em;

	private ArrayList<Element> elements;

	private Element selectedElement = null;
	private TreeMap<String, String> labMap;

	public InspectFormUI(ArrayList<Element> el, TreeMap<String, String> labMap) {
		super("Inspect UI");

		/*
		 * Starting JPA
		 */
		em = JPAUtil.getInstance().getEntityManager();
		em.getTransaction().begin();

		/*
		 * Setting up the first element
		 */
		elements = el;
		this.labMap = labMap;

		if (elements != null && !elements.isEmpty()) {
			selectedElement = elements.remove(0);
		}

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				closeEM();
			}
		});

		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		JPanel jp1 = new JPanel();
		JPanel jp2 = new JPanel();
		JPanel jp3 = new JPanel();
		JPanel jp4 = new JPanel();
		JPanel jp5 = new JPanel();
		JPanel jp6 = new JPanel();

		getContentPane().add(jp1);
		getContentPane().add(jp2);
		
		JLabel lblUiLabel = new JLabel("UI Label:");
		jp2.add(lblUiLabel);
		
		txtLabel = new JTextField();
		jp2.add(txtLabel);
		txtLabel.setColumns(40);
		getContentPane().add(jp3);
		getContentPane().add(jp4);
		getContentPane().add(jp5);
		getContentPane().add(jp6);

		jp1.add(new JLabel("Company:"));
		jp1.add(ciaCombo);
		
		JLabel lblSeguroLinkLabel = new JLabel("Seguro Link Label:");
		jp1.add(lblSeguroLinkLabel);
		
		jp1.add(comboLabelSeguroLink);

		jp3.add(new JLabel("UI Field ID: "));
		jp3.add(ufIdTF);
		jp3.add(new JLabel("Type:"));
		jp3.add(ufTypeCompo);
		jp3.add(new JLabel("Create New:"));
		jp3.add(ufTypeTF);
		
		JLabel lblLocateBy = new JLabel("Locate by:");
		jp4.add(lblLocateBy);
		
		jp4.add(locateCombo);

		jp4.add(new JLabel("Order"));
		jp4.add(ufOrderTF);
		jp4.add(ufCheck);

		jp5.add(new JLabel("Mandatory Condition"));
		jp5.add(mcCombo);
		jp5.add(new JLabel("Value"));
		jp5.add(mcTF);

		JButton skip = new JButton("Skip");
		skip.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				skip();

			}
		});
		JButton save = new JButton("Save");

		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				save();

				skip();
			}
		});

		JButton refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
		JButton end = new JButton("End");

		end.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				closeEM();

			}
		});

		jp6.add(skip);
		jp6.add(save);
		jp6.add(refresh);
		jp6.add(end);

		refresh();
		loadSelectedElement();

		pack();
		setVisible(true);
	}

	private SourceUITypeSelect typeSelect = null;
	private JTextField txtLabel;

	private void loadSelectedElement() {
		typeSelect = null;
		if (selectedElement == null) {
			return;
		}
		Element el = selectedElement;
		ufTypeTF.setText("");
		txtLabel.setText("");


		/*
		 * id
		 */
		ufIdTF.setText(el.attr("id"));
		
		if (labMap.get(el.attr("id")) != null) {
			txtLabel.setText(labMap.get(el.attr("id"))+"");
			locateCombo.setSelectedItem(FindElementBy.ID);
		}

		switch (el.tagName()) {
		case "input":
			String classHtml = el.attr("class");
			ufTypeTF.setText(classHtml);

			break;
		case "select":
			ufTypeTF.setText(SELECT_TYPE);

			typeSelect = new SourceUITypeSelect();
			typeSelect.setLabel("Select");

			Elements options = el.select("option");
			for (Element opt : options) {
				SourceUITypeSelectItem option = new SourceUITypeSelectItem();
				option.setText(opt.text());
				option.setValue(opt.attr("value"));
				typeSelect.getOptions().add(option);
				
			}

			break;

		}

		refresh();
	}

	private void refresh() {
		InsuranceCompany[] companies = InsuranceCompany.values();
		
		ciaCombo.removeAllItems();
		for (int i = 0; i < companies.length; i++) {
			ciaCombo.addItem(new ComboHolder<InsuranceCompany>(companies[i], companies[i].toString()));
		}
		
		FindElementBy[] findElementsBy = FindElementBy.values();
		locateCombo.removeAllItems();
		for (int i = 0; i < findElementsBy.length; i++) {
			locateCombo.addItem(new ComboHolder<FindElementBy>(findElementsBy[i], findElementsBy[i].toString()));
		}
		
		List<Field> seguroLinkFields = (List<Field>) new FieldDao().listAll();
		comboLabelSeguroLink.removeAllItems();
		comboLabelSeguroLink.addItem(new ComboHolder<Field>(null, "none"));
		for (Field field : seguroLinkFields) {
			comboLabelSeguroLink.addItem(new ComboHolder<Field>(field, field.getLabel()));
		}

		List<SourceField> fields = em.createQuery("from SourceField source")
				.getResultList();
		mcCombo.removeAllItems();
		mcCombo.addItem(new ComboHolder<SourceField>(null, "none"));
		for (SourceField ui : fields) {
			mcCombo.addItem(new ComboHolder<SourceField>(ui, "DOMAINHARDCODE"));
		}

		List<SourceUIType> types = em.createQuery("from SourceUIType ui").getResultList();
		ufTypeCompo.removeAllItems();
		for (SourceUIType ui : types) {
			ufTypeCompo.addItem(new ComboHolder<SourceUIType>(ui, ui.getLabel()));
		}

		repaint();

	}

	public static void main(String a[]) {
		new InspectFormUI(null, null);
	}

	private void closeEM() {
		System.out.println("Close Entity Management");
		try {
			em.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}

		this.dispose();
	}

	private void skip() {
		if (elements != null && !elements.isEmpty()) {
			selectedElement = elements.remove(0);
			loadSelectedElement();
		} else {
			JOptionPane.showMessageDialog(InspectFormUI.this,
					"No more elements to map!");
		}
	}

	private void showMessageErrorAndRollback(String message) {
		JOptionPane.showMessageDialog(InspectFormUI.this, message);
		em.getTransaction().rollback();

		em.getTransaction().begin();
	}

	private void save() {

		try {

			SourceUIType sourceUIType = null;
			if (typeSelect != null) {
				sourceUIType = typeSelect;
				for (SourceUITypeSelectItem item: typeSelect.getOptions()) {
					em.persist(item);
				}
				
				em.persist(sourceUIType);
			} else {
				if (!ufTypeTF.getText().trim().equals("")) {
					sourceUIType = new SourceUIType();
					sourceUIType.setLabel(ufTypeTF.getText().trim());
					em.persist(sourceUIType);
				} else if (ufTypeCompo.getSelectedItem() != null) {
					sourceUIType = ((ComboHolder<SourceUIType>) ufTypeCompo
							.getSelectedItem()).getValue();
				} else {
					String message = "Please inform a UI type";
					showMessageErrorAndRollback(message);
					return;
				}
			}

			if (ufIdTF.getText().trim().equals("")) {
				String message = "Please inform a UI ID";
				showMessageErrorAndRollback(message);
				return;
			}

			SourceMandatoryCondition mc = null;

			if (!mcTF.getText().trim().equals("")
					&& mcCombo.getSelectedItem() != null) {
				mc = new SourceMandatoryCondition();
				mc.setValue(mcTF.getText());
				mc.setIfField(((ComboHolder<SourceField>) mcCombo.getSelectedItem())
						.getValue());
				em.persist(mc);
			}

			SourceField sourceField = new SourceField();
			sourceField.setSourceUIIdentifier(ufIdTF.getText().trim());
			sourceField.setIsMandatory(ufCheck.isSelected());
			sourceField.setEntryOrder(new Integer(ufOrderTF.getText().trim()));
			sourceField.setMandatoryCondition(mc);
			sourceField.setType(sourceUIType);
			sourceField.setCompany( ((ComboHolder<InsuranceCompany>) ciaCombo.getSelectedItem()).getValue() );
			sourceField.setFindElementBy( ((ComboHolder<FindElementBy>) locateCombo.getSelectedItem()).getValue() );
			sourceField.setLabel(txtLabel.getText().trim());
			sourceField.setField( ((ComboHolder<Field>) comboLabelSeguroLink.getSelectedItem()).getValue() );
			

			em.persist(sourceField);

			em.getTransaction().commit();
			em.getTransaction().begin();
		} catch (Exception ex) {
			ex.printStackTrace();
			em.getTransaction().rollback();
			em.getTransaction().begin();
		}
	}

}
