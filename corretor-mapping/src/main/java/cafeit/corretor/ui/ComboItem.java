package cafeit.corretor.ui;

import org.openqa.selenium.WebElement;




public class ComboItem {
	
	private WebElement value;
    private String label;

    
    
    public ComboItem(WebElement el) {
    	value = el;
    	label = el.getTagName();
    	if (el.getText()!=null) {
    		label = label+" "+el.getText();
    	}
    	for (String atr : new String[] {"value","title","id"}) {
    		if (el.getAttribute(atr) != null) {
    			label = label+" | "+atr+" "+el.getAttribute(atr);
    		}
    	}
    }

    public WebElement getValue() {
        return this.value;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return label;
    }

}
