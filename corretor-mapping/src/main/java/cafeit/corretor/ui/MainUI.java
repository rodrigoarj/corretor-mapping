package cafeit.corretor.ui;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import cafeit.corretor.util.JPAUtil;
import cafeit.corretor.util.JSoupUtil;

public class MainUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WebDriver driver;

	/*
	 * UI Components
	 */
	private JTextField openTf = new JTextField(
			"https://www.maritimaweb.com.br/wps/portal/", 30);
	private JTextField searchElementsTf = new JTextField(20);
	private JComboBox<ComboItem> listOfElements = new JComboBox<>();
	private JComboBox<String> listOfTabs = new JComboBox<>();

	public MainUI() {
		super("Insurance Screen Mapping");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		buildUI();
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (driver != null) {
					try {
						driver.close();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
	}

	private void buildUI() {
		setLayout(new BorderLayout());

		JPanel north = new JPanel();
		JPanel center = new JPanel();
		JPanel south = new JPanel();
		add(north, BorderLayout.NORTH);
		add(center, BorderLayout.CENTER);
		add(south, BorderLayout.SOUTH);

		north.add(new JLabel("Open new:"));
		north.add(openTf);
		JButton openBt = new JButton("Go!");
		north.add(openBt);
		JButton listAllInputBt = new JButton("List All Inputs");
		north.add(listAllInputBt);

		/*
		 * North actions
		 */

		openBt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (openTf.getText().trim().equals("")) {
					return;
				}
				if (driver == null) {
					/*
					 * When null, create a new, otherwise reuse the existing one
					 */
					FirefoxProfile profile = new FirefoxProfile();
					profile.setPreference("webdriver.firefox.bin",
							"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
					driver = new FirefoxDriver(profile);
				}

				driver.get(openTf.getText().trim());

				loadingPage();

			}
		});

		listAllInputBt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (driver == null) {
					return;
				}

				new JSoupUtil().extractElements(driver.getPageSource());
				new JSoupUtil().extractElementsIntoDatabase(driver
						.getPageSource());

			}
		});

		center.add(new JLabel("Search XPath:"));
		center.add(searchElementsTf);
		JButton searchBt = new JButton("Search");
		center.add(searchBt);

		searchBt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (driver == null
						|| searchElementsTf.getText().trim().equals("")) {
					return;
				}

				String s = searchElementsTf.getText().trim();

				List<WebElement> els = driver.findElements(By.xpath(s));

				listOfElements.removeAllItems();
				for (WebElement el : els) {
					listOfElements.addItem(new ComboItem(el));
				}

			}
		});

		center.add(listOfElements);
		JButton click = new JButton("Click");
		center.add(click);
		click.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (listOfElements.getSelectedItem() == null) {
					return;
				}
				ComboItem cb = (ComboItem) listOfElements.getSelectedItem();
				cb.getValue().click();

				loadingPage();
			}
		});

		JButton showResultadoMar = new JButton("Show Resultado Maritima");
		center.add(showResultadoMar);

		showResultadoMar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});

		south.add(new JLabel("Window Handlers"));
		JButton refWindow = new JButton("Refresh");
		south.add(refWindow);
		refWindow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refreshAllOpenedWindows();

			}
		});

		south.add(listOfTabs);
		JButton selectTab = new JButton("Select Window");
		selectTab.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (listOfTabs == null || listOfTabs.getSelectedItem() == null
						|| driver == null) {
					return;
				}

				driver.switchTo().window(
						listOfTabs.getSelectedItem().toString());

			}
		});
		south.add(selectTab);

		setSize(800, 400);
		setVisible(true);

	}

	private void waitPageLoaded() {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return      document.readyState").equals("complete");
			}
		};
		Wait<WebDriver> wait = new WebDriverWait(driver, 60);
		wait.until(expectation);
	}

	private void refreshAllOpenedWindows() {
		Set<String> winds = driver.getWindowHandles();

		listOfTabs.removeAllItems();
		for (String w : winds) {
			listOfTabs.addItem(w);
		}
		MainUI.this.repaint();

		listOfTabs.setSelectedItem(driver.getWindowHandle());
	}

	private void loadingPage() {
		waitPageLoaded();

		System.out.println("loaded!");

		refreshAllOpenedWindows();
	}

	@SuppressWarnings("unused")
	private void listInputBySelenium() {
		System.out.println("List all inputs");
		List<WebElement> allFormChildElements = driver.findElements(By
				.tagName("input"));
		for (WebElement el : allFormChildElements) {
			if (el.getTagName().equals("input")) {

				System.out.println("Type " + el.getAttribute("type") + " | ID "
						+ el.getAttribute("id") + " | Name "
						+ el.getAttribute("id"));
			}
		}
	}

	public static void main(String[] args) {
		new MainUI();
	}

}
