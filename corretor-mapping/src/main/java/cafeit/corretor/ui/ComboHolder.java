package cafeit.corretor.ui;


public class ComboHolder<T> {

	@SuppressWarnings("unused")
	private T value;

	private String label;

	public ComboHolder(T v, String l) {
		this.value = v;
		this.label = l;

	}
	
	public T getValue() {
		return value;
	}

	public String toString() {
		return label;
	}

}
