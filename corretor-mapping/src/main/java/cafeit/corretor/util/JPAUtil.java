package cafeit.corretor.util;


import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static JPAUtil instance;

	private static String PERSISTENCE_UNIT = "corretor_PU";

	private static EntityManagerFactory factory;

	private TreeMap<String, EntityManagerFactory> factories = new TreeMap<String, EntityManagerFactory>();

	private JPAUtil() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		factories.put(PERSISTENCE_UNIT, factory);
	}

	public Object execute(JPAExec block, String persistenceUnit)
			throws Exception {
		EntityManager em;
		if (persistenceUnit == null) {
			em = factory.createEntityManager();
		} else {
			EntityManagerFactory otherFactory = null;
			if (factories.containsKey(persistenceUnit)) {
				otherFactory = factories.get(persistenceUnit);
			} else {
				otherFactory = Persistence
						.createEntityManagerFactory(persistenceUnit);
				factories.put(persistenceUnit, otherFactory);
			}

			em = otherFactory.createEntityManager();
		}

		try {
			em.getTransaction().begin();

			Object result = block.execute(em);

			em.getTransaction().commit();

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return null;

	}

	public Object execute(JPAExec block) throws Exception {
		return execute(block, null);
	}

	public EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

	public synchronized static JPAUtil getInstance() {
		if (instance == null) {
			instance = new JPAUtil();
		}

		return instance;
	}
	
	public static void main(String[] args) {
		JPAUtil.getInstance().getEntityManager();
	}
	
}
