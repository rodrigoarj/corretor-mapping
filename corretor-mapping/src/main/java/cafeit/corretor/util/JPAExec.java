package cafeit.corretor.util;

import javax.persistence.EntityManager;

public interface JPAExec {
	
	public Object execute(EntityManager em) throws Exception;

}
