package cafeit.corretor.util;



import java.util.ArrayList;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cafeit.corretor.ui.InspectFormUI;

public class JSoupUtil {

	public void extractElementsIntoDatabase(String pageSource) {
		Document root = Jsoup.parse(pageSource);

		Elements labels = root.select("label");
		TreeMap<String, String> labMap = new TreeMap<>();

		for (Element lbl : labels) {
			labMap.put(lbl.attr("for"), lbl.text());
		}

		Elements all = root.getAllElements();

		ArrayList<Element> elements = new ArrayList<>();

		for (Element el : all) {

			switch (el.tagName()) {
			case "input":
				String typeInput = el.attr("type");
				if (typeInput != null && typeInput.equals("hidden")) {
					continue;
				}
				elements.add(el);
				break;
			case "select":
				elements.add(el);
				break;

			}

		}
		
		new InspectFormUI(elements, labMap);

	}

	public void extractElements(String pageSource) {
		Document root = Jsoup.parse(pageSource);

		Elements labels = root.select("label");
		TreeMap<String, String> labMap = new TreeMap<>();

		for (Element lbl : labels) {
			labMap.put(lbl.attr("for"), lbl.text());
		}

		Elements all = root.getAllElements();

		for (Element el : all) {

			switch (el.tagName()) {
			case "input":
				String typeInput = el.attr("type");
				if (typeInput != null && typeInput.equals("hidden")) {
					continue;
				}
				String classHtml = el.attr("class");
				System.out.println(labMap.get(el.attr("id")) + "\t"
						+ el.attr("id") + "\tInput Text");

				System.out.println("\tClass: " + classHtml + " Type: "
						+ typeInput);
				System.out.println("--END--");

				break;
			case "select":
				System.out.println(labMap.get(el.attr("id")) + "\t"
						+ el.attr("id") + "\tSelect");
				System.out.println("\tOptions: ");
				Elements options = el.select("option");
				for (Element opt : options) {
					System.out.println(opt.text() + "|" + opt.attr("value")
							+ "|" + opt.attr("selected"));
				}
				System.out.println("--END--");
				break;

			}

		}

	}
	
	public static void main(String[] args) {
		
	}
}
